# Reindeer : index et consommation
Créée le mardi 08 septembre 2020


Pour rappel, ce tableau résume la quantité de mémoire nécessaire pour nos indexes :

| Index name               |Fastq size (GB)|index size (GB)|RAM size (GB)|Load time (m:s)|sample count| read count|total kmer count|Distinct kmer count¹|Trimmed|Stranded| Type |
|:-------------------------|:--------------|:--------------|:------------|:--------------|:-----------|:----------|:---------------|:-------------------|:------|:-------|:-----|
|PF-2009-Bisbal            |          46   |       0.9     |        2.1  |    00:00:16   |       18   |     NA    |    133E+09     |           NA       |   no  |  yes   |single|
|CD4-ALK                   |          75   |       0.9     |        2.2  |    00:00:19   |       13   |  10068E+06|   1327E+09     |         198E+06    |   no  |  yes   |paired|
|GSE135902-CMML-12         |          20   |       1.2     |        3.0  |    00:00:32   |       12   |    556E+06|     11E+09     |         236E+06    |   no  |   no   |single|
|LMMC-genoscope-20         |         126   |       1.6     |        3.7  |    00:00:29   |       20   |     NA    |    123E+09     |           NA       |   no  |  yes   |paired|
|Leucegene-CD34+           |         111   |       1.6     |        3.8  |    00:00:31   |       17   |    740E+06|    103E+09     |         342E+06    |  yes  |   no   |paired|
|CCLE-7-prostate-cutadapt  |          58   |       1.1     |        2.1  |    00:00:20   |        7   |     NA    |     71E+09     |           NA       |  yes  |   no   |paired|
|CCLE-7-prostate           |          66   |       3.7     |        6.9  |    00:01:20   |        7   |   1072E+06|     85E+09     |         954E+06    |   no  |   no   |paired|
|TGCA-RNAseq-38            |         204   |       3.8     |        8.6  |    00:01:07   |       38   |   2171E+06|    133E+09     |         352E+06    |   no  |   no   |paired|
|PRJNA307572-Lecellier-140 |         154   |       4.8     |       10.2  |    00:01:09   |      140   |   2997E+06|    132E+09     |         176E+06    |   NA  |  mix   |single|
|CRCT-Marina-27-hemato     |         261   |       5       |        8.8  |    00:01:36   |       27   |   1485E+06|    242E+09     |        1069E+06    |   no  |  yes   |paired|
|JE-Sarry-36-AML           |         239   |       5.3     |       10.0  |    00:01:38   |       36   |   3034E+06|    335E+09     |         706E+06    |   no  |  yes   |paired|
|GSE62852-40-AML           |         300   |       6.7     |       12.5  |    00:02:17   |       40   |   3205E+06|    192E+09     |         781E+06    |   no  |  yes   |paired|
|Hepatoblastome-61         |         500   |       8       |       14.4  |    00:02:46   |       61   |   2946E+06|    560E+09     |         982E+06    |   no  |  yes   |paired|
|LMMC-Solary-RNASeq-2018   |         320   |      11       |       19.4  |    00:03:23   |       77   |   4367E+06|    393E+09     |         710E+06    |   no  |  yes   |paired|
|ALCL-fullRNA-19-Meggetto  |         820   |      13       |       21.8  |    00:04:06   |       19   |   9760E+06|   1181E+09     |        2155E+06    |   no  |  yes   |paired|
|JE-Sarry-44-AML           |         275   |      15       |       30.6  |    00:09:06   |       44   |   2751E+06|    385E+09     |        2130E+06    |   no  |  yes   |paired|
|LUAD-seo                  |         969   |      38       |       25.0  |    00:10:30   |      154   |  11200E+06|    713E+09     |        2840E+06    |  yes  |   no   |paired
|CCLE-57-breast            |         568   |      27       |       47.4  |    00:09:10   |       57   |   5071E+06|    720E+09     |        3120E+06    |   no  |   no   |paired|
|CCLE-58-haemato-myeloid   |         587   |      27       |       48.1  |    00:08:50   |       58   |   5185E+06|    740E+09     |        3105E+06    |   no  |   no   |paired|
|Leucegene-metadata-148    |        2500   |      27       |       49.6  |    00:07:06   |      148   |  15624E+06|   2130€+09     |        1200E+06    |  yes  |   no   |paired|
|Leucegene-non-stranded-203|        3200   |      40       |       70.1  |    00:12:11   |      203   |  20413E+06|   2800E+09     |        1368E+06    |  yes  |   no   |paired|
|Leucegene-stranded-251    |        2000   |      49       |       85.8  |    00:10:23   |      251   |  25140E+06|   3520E+09     |        1317E+06    |  yes  |  yes   |paired|
|ALCL-fullRNA-48-megetto   |        2000   |      42       |       77.7  |    00:11:32   |       48   |  24163E+06|   2923E+09     |        4402E+06    |   no  |  yes   |paired|
|CCLE-94-1-lung            |        1008   |      45       |       85.3  |    00:14:29   |       94   |   8967E+06|   1270E+09     |        4182E+06    |   no  |   no   |paired|
|CCLE-126_mixed            |        1300   |      59       |      104.5  |    00:20:00   |      126   |  11322E+06|   1600E+09     |        4767E+06    |   no  |   no   |paired|
|CCLE-mix-126 (cutadapt)   |        1100   |      17       |       29.8  |    00:04:04   |      126   |  10068E+06|   1330E+09     |        1159E+06    |  yes  |   no   |paired|
|CCLE-140-haemato          |        1500   |      68       |      118.9  |    00:17:09   |      140   |  12931E+06|   1800E+09     |        4938E+06    |   no  |   no   |paired|
|CCLE-188-lung             |        2000   |      93       |      174.9  |    00:26:17   |      188   |  15904E+06|   2500E+09     |        5894E+06    |   no  |   no   |paired|
|CCLE-188-lung (cutadapt)  |        1800   |      28       |       47    |    00:06:00   |      188   |  15904E+06|   2105E+09     |        1274E+06    |  yes  |   no   |paired|
|CCLE-335-hematoLungPros   |        3500   |     163       |      286.2  |    00:43:34   |      335   |  31260E+06|   4400E+09     |        8074E+06    |   no  |   no   |paired|
|CCLE-540-misc             |        5500   |     252       |      436    |    01:11:00   |      540   |  31260E+06|   4440E+09     |       10063E+06    |   no  |   no   |paired|
|CCLE-1019-cutadapt        |        8900   |     186       |      255.6  |    00:31:05   |     1019   |  81956E+06|  10806E+09     |       13763E+06    |  yes  |   no   |paired|
|CCLE-1019                 |       11000   |     NA        |      NA     |       NA      |     1019   |  91993E+06|  13061E+09     |          NA        |   no  |   no   |paired|




## Résumé des index du CRCT

| Index name                   | Fastq files (GB) |Index size (GB)| Index RAM (GB) |Load time (m:s)|
|:-----------------------------|:-----------------|:--------------|:---------------|:--------------|
| CD4-ALK-13                   |       75         |     1.1       |       1.5      |       00:19   |
| CRCT-Marina-27-hemato        |      261         |     5.8       |       9        |       01:36   |
| JE-Sarry-44-AML              |      275         |    15         |      30.6      |       09:06   |
| GSE62852-40-AML              |      300         |     6.7       |      12.8      |       02:17   |
| ALCL-fullRNA-19-Meggetto     |      820         |     15        |      22.4      |       04:06   |
| ALCL-fullRNA-48-Meggetto     |     2000         |     42        |      70        |       11:32   |
| Leucegene-metadata-148       |     2500         |     27        |      49.6      |       07:06   |
| Leucegene-non-stranded-203   |     3200         |     40        |      70.1      |       12:11   |
| Leucegene-stranded-251       |     2000         |     49        |      85.8      |       10:23   |
| CCLE-mix-126 (trimmed)       |     1100         |     17        |      30        |        7:00   |
| CCLE-1019-cutadapt           |     8900         |    186        |     262        |       40:00   |
